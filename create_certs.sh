#!/bin/bash
set -e

for crt in 1 2; do
    # Create root CA
    openssl genrsa -out rootCA_$crt.key 4096
    openssl req -x509 -new -nodes \
        -key rootCA_$crt.key -sha256 -days 3650 -out rootCA_$crt.crt \
        -subj "/C=US/ST=CA/O=Org $crt/CN=root" \
        -addext "keyUsage = keyCertSign, cRLSign, digitalSignature"

    # Create server key and CSR
    openssl genrsa -out server_$crt.key 2048
    openssl req -new -sha256 -key server_$crt.key \
        -subj "/C=US/ST=CA/O=Org $crt/CN=localhost" \
        -addext "subjectAltName = DNS:localhost" \
        -out server_$crt.csr

    # Sign CSR
    openssl x509 -req \
        -extfile <(printf "subjectAltName = DNS:localhost") \
        -in server_$crt.csr -CA rootCA_$crt.crt -CAkey rootCA_$crt.key \
        -CAcreateserial \
        -out server_$crt.crt -days 500 -sha256

    openssl verify -x509_strict -CAfile rootCA_$crt.crt server_$crt.crt
done
