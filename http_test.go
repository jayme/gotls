package main

import (
	"crypto/tls"
	"crypto/x509"
	"net/http"
	"os"
	"testing"
)

func TestHTTPServerWithTLS(t *testing.T) {
	// Load the server certificate and private key
	cert, err := tls.LoadX509KeyPair("server_1.crt", "server_1.key")
	if err != nil {
		t.Fatalf("failed to load server certificate and private key: %v", err)
	}

	// Create a TLS configuration with the server certificate
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
	}

	// Create the HTTP server with the TLS configuration
	server := &http.Server{
		Addr:      ":18443",
		TLSConfig: tlsConfig,
	}

	// Start the HTTP server in a separate goroutine
	go func() {
		if err := server.ListenAndServeTLS("", ""); err != http.ErrServerClosed {
			t.Errorf("failed to start HTTP server: %v", err)
		}
	}()
	defer server.Close()

	t.Run("TestCertOK", func(t *testing.T) {
		// Load the CA certificate
		caCert, err := os.ReadFile("rootCA_1.crt")
		if err != nil {
			t.Fatalf("failed to load CA certificate: %v", err)
		}

		// Create a certificate pool and add the CA certificate
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		// Create an HTTP client with the CA certificate pool
		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					RootCAs: caCertPool,
				},
			},
		}
		// Make a request to the server
		resp, err := client.Get("https://localhost:18443")
		if err != nil {
			t.Fatalf("failed to make request to server: %v", err)
		}
		defer resp.Body.Close()
	})

	t.Run("TestCertErr", func(t *testing.T) {
		// Load the CA certificate
		caCert, err := os.ReadFile("rootCA_2.crt")
		if err != nil {
			t.Fatalf("failed to load CA certificate: %v", err)
		}

		// Create a certificate pool and add the CA certificate
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)
		// Create an HTTP client with the CA certificate pool
		client := &http.Client{
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					RootCAs: caCertPool,
				},
			},
		}
		// Make a request to the server
		resp, err := client.Get("https://localhost:18443")
		if err == nil {
			defer resp.Body.Close()
			t.Fatal("certificate verification should have failed")
		}
	})
}
