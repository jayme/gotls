package main

import (
	"crypto/tls"
	"crypto/x509"
	"net"
	"os"
	"testing"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

func TestGRPCServerWithTLS(t *testing.T) {
	// Load the server certificate and private key
	cert, err := tls.LoadX509KeyPair("server_1.crt", "server_1.key")
	if err != nil {
		t.Fatalf("failed to load server certificate and private key: %v", err)
	}

	// Create a TLS configuration with the server certificate
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
	}

	// Create a new gRPC server with TLS
	server := grpc.NewServer(grpc.Creds(credentials.NewTLS(tlsConfig)))

	// Create a listener for the gRPC server
	listener, err := net.Listen("tcp", ":9000")
	if err != nil {
		t.Fatalf("failed to create listener: %v", err)
	}

	// Start the gRPC server in a separate goroutine
	go func() {
		if err := server.Serve(listener); err != nil {
			t.Errorf("failed to start gRPC server: %v", err)
		}
	}()
	defer server.Stop()

	t.Run("TestCertOK", func(t *testing.T) {
		// Load the CA certificate
		caCert, err := os.ReadFile("rootCA_1.crt")
		if err != nil {
			t.Fatalf("failed to load CA certificate: %v", err)
		}

		// Create a certificate pool and add the CA certificate
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)

		tlsConfig := &tls.Config{
			RootCAs:    caCertPool,
			ClientAuth: tls.NoClientCert,
		}

		conn, err := grpc.Dial("localhost:9000", grpc.WithTransportCredentials(credentials.NewTLS(tlsConfig)))
		if err != nil {
			t.Fatalf("failed to dial gRPC server: %v", err)
		}
		defer conn.Close()
	})

	t.Run("TestCertErr", func(t *testing.T) {
		// Load the CA certificate
		caCert, err := os.ReadFile("rootCA_2.crt")
		if err != nil {
			t.Fatalf("failed to load CA certificate: %v", err)
		}

		// Create a certificate pool and add the CA certificate
		caCertPool := x509.NewCertPool()
		caCertPool.AppendCertsFromPEM(caCert)

		tlsConfig := &tls.Config{
			RootCAs:    caCertPool,
			ClientAuth: tls.NoClientCert,
		}

		conn, err := grpc.Dial("localhost:9000", grpc.WithTransportCredentials(credentials.NewTLS(tlsConfig)))
		if err == nil {
			defer conn.Close()
			t.Fatal("certificate verification should have failed")
		}
	})
}
